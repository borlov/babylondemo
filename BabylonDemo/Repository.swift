//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation
import Domain

public class Repository: RepositoryProtocol {
    let networkService: NetworkServiceProtocol
    let cacheStorage: UserDefaults

    public init(networkService: NetworkServiceProtocol, cacheStorage: UserDefaults = UserDefaults.standard) {
        self.networkService = networkService
        self.cacheStorage = cacheStorage
    }

    public func fetch<E: Entity>(completion: @escaping (FetchResult<E>) -> Void) {
        self.fetch(predicate: nil, completion: completion)
    }

    public func fetch<E:Entity>(predicate: NSPredicate?, completion: @escaping (FetchResult<E>) -> Void) {
        let request = Request(endpoint: self.endpoint(for: E.self))
        if let cachedJSON = self.cachedJSON(for: request) {
            let entities: [E] = self.entities(from: cachedJSON, filterUsing: predicate)
            completion(FetchResult.some(entities, fromCache: true))
        }
        networkService.send(request: request) { response in
            switch response {
                case .success(let data):
                    guard let json = try? JSONSerialization.jsonObject(with: data),
                          let jsonArray = self.jsonArray(from: json) else {
                        completion(FetchResult.error(NSError(domain: String(describing: self), code: -1)))
                        return
                    }
                    self.cache(json: jsonArray, for: request)
                    let entities: [E] = self.entities(from: jsonArray, filterUsing: predicate)
                    completion(FetchResult.some(entities, fromCache: false))
                case .failure(let error):
                    completion(FetchResult.error(error))
            }
        }
    }

    private func jsonArray(from foundationJSON: Any) -> [JSON]? {
        if let json = foundationJSON as? JSON {
            return [json]
        }
        if let json = foundationJSON as? [JSON] {
            return json
        }
        return nil
    }

    private func entities<E:Entity>(from json: [JSON], filterUsing predicate: NSPredicate?) -> [E] {
        var filteredJSONArray = json
        if let predicate = predicate {
            filteredJSONArray = json.filter { predicate.evaluate(with: $0)}
        }
        let entities = filteredJSONArray.map { E.init(json: $0) }
        return entities
    }

    private func cache(json: [JSON], for request: Request) {
        self.cacheStorage.setValue(json, forKey: request.endpoint)
    }

    private func cachedJSON(for request: Request) -> [JSON]? {
        return self.cacheStorage.value(forKey: request.endpoint) as? [JSON]
    }

    private func endpoint<E: Entity>(for entity: E.Type) -> String {
        switch entity {
            case is Post.Type: return "posts"
            case is Coordinates.Type: return "coordinates"
            case is Comment.Type: return "comments"
            case is User.Type: return "users"
            default: assert(false, "Unexpected Entity type: \(entity)")
        }
    }
}
