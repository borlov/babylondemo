//
//  PostsViewController.swift
//  BabylonDemo
//
//  Created by Bohdan Orlov on 08/12/2016.
//  Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import UIKit
import Platform

class MasterViewController: UITableViewController {

    var detailViewController: PostDetailsViewController? = nil
    var viewModel: PostsListViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Ideally VC should only know about VM, but in this demo VC is used as an assembly point
        let hostURL = URL(string: "http://jsonplaceholder.typicode.com/")!
        let networkService = NetworkService(hostURL: hostURL, session: URLSession.shared)
        let repository = Repository(networkService: networkService)
        self.viewModel = PostsListViewModel(repository: repository)
        self.viewModel.didChange = { [weak self] viewModel in
            self?.tableView.reloadData()
            viewModel.isFetching ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
        }

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count - 1] as! UINavigationController).topViewController as? PostDetailsViewController
        }

        self.prepareActivityIndicator()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        self.viewModel.willAppear()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! PostDetailsViewController
                controller.viewModel = self.viewModel.detailsViewModetForSelectedPostTitle(at: indexPath.row)
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.postTitles.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let postTitle = self.viewModel.postTitles[indexPath.row]
        self.configureCell(cell, with: postTitle)
        return cell
    }

    private func configureCell(_ cell: UITableViewCell, with postTitle: String) {
        cell.textLabel!.text = postTitle
    }

    private func prepareActivityIndicator() {
        self.activityIndicator.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.activityIndicator)
    }
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
}
