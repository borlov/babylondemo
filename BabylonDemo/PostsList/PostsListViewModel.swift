//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation
import Domain
import Platform

public class PostsListViewModel {
    public let repository: RepositoryProtocol
    public var postTitles: [String] {
        return self.posts.map { $0.title }
    }
    public var isFetching = false {
        didSet {
            self.didChange?(self)
        }
    }
    public var didChange: ((PostsListViewModel) -> Void)?
    public init(repository: RepositoryProtocol) {
        self.repository = repository
    }

    public func willAppear() {
        self.isFetching = true
        self.repository.fetch { [weak self] (result: FetchResult<Post>) in
            guard let sSelf = self else { assert(false); return }
            switch result {
                case .some(let posts, let fromCache):
                    sSelf.posts = posts;
                    sSelf.didChange?(sSelf)
                    if !fromCache {
                        sSelf.isFetching = false
                    }
                case .error:
                    sSelf.isFetching = false
            }
        }
    }
    private var posts: [Post] = [Post]() {
        didSet {
            self.didChange?(self)
        }
    }

    public func detailsViewModetForSelectedPostTitle(at index: Int) -> PostDetailsViewModel {
        return PostDetailsViewModel(post: self.posts[index], repository: self.repository)
    }
}

