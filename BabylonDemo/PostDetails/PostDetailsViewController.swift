//
//  PostDetailsViewController.swift
//  BabylonDemo
//
//  Created by Bohdan Orlov on 08/12/2016.
//  Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import UIKit


class PostDetailsViewController: UIViewController {
    public var viewModel: PostDetailsViewModel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var postDescriptionLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.authorLabel.text = nil
        self.commentsCountLabel.text = nil
        self.postDescriptionLabel.text = viewModel.postDescription
        self.prepareActivityIndicator()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.willAppear()
        self.viewModel.didChange = { [weak self] viewModel in
            self?.authorLabel.text = viewModel.authorName
            self?.commentsCountLabel.text = "\(viewModel.commentsCount)"
            viewModel.isFetching ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
        }

    }

    private func prepareActivityIndicator() {
        self.activityIndicator.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.activityIndicator)
    }
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
}
