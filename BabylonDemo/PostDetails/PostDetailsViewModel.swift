//
// Created by Bohdan Orlov on 11/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation
import Domain
import Platform

public class PostDetailsViewModel {
    public let post: Post
    public let repository: RepositoryProtocol
    public var postDescription: String {
        return self.post.body
    }
    public var authorName: String? {
        return self.author?.name
    }
    public var commentsCount: Int {
        return self.comments.count
    }
    public var isFetching: Bool {
        return self.activeFetchesCount > 0
    }
    public var didChange: ((PostDetailsViewModel) -> Void)?

    public init(post: Post, repository: RepositoryProtocol) {
        self.post = post
        self.repository = repository
    }

    public func willAppear() {
        self.activeFetchesCount += 1
        repository.fetch(predicate: User.id == post.userId) { [weak self] (result: FetchResult<User>) in
            switch result {
                case .some(let user, let fromCache):
                    self?.author = user.first
                    if !fromCache {
                        self?.activeFetchesCount -= 1
                    }
                case .error: break
            }
        }
        self.activeFetchesCount += 1
        repository.fetch(predicate: Comment.postId == post.id) { [weak self] (result: FetchResult<Comment>) in
            switch result {
            case .some(let comments,  let fromCache):
                self?.comments = comments
                if !fromCache {
                    self?.activeFetchesCount -= 1
                }
            case .error:
                self?.activeFetchesCount -= 1
            }
        }
    }
    private var activeFetchesCount = 0 {
        didSet {
            self.didChange?(self)
        }
    }
    private var author: User? {
        didSet {
            self.didChange?(self)
        }
    }
    private var comments: [Comment] = [Comment]() {
        didSet {
            self.didChange?(self)
        }
    }
}
