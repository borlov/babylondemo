//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public typealias JSON = [String: Any]

public protocol JSONSerializable {
    init(json: JSON)
    var asJSON: JSON { get }
}
