//
// Created by Bohdan Orlov on 09/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Address: Entity {
    public let street: String
    public let suite: String
    public let city: String
    public let zipcode: String
    public let geo: Coordinates
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(street: String, suite: String, city: String, zipcode: String, geo: Coordinates) {
        self.street = street
        self.suite = suite
        self.city = city
        self.zipcode = zipcode
        self.geo = geo
    }
}

extension Address {
    public static var street:Attribute<String> { return "street" }
    public static var suite:Attribute<String> { return "suite" }
    public static var city:Attribute<String> { return "city" }
    public static var zipcode:Attribute<String> { return "zipcode" }
    public static var geo:Attribute<String> { return "geo" }
}

extension Address {
    public init(json: JSON) {
        self.street = json[Address.street.key] as? String ?? ""
        self.suite = json[Address.suite.key] as? String ?? ""
        self.city = json[Address.city.key] as? String ?? ""
        self.zipcode = json[Address.zipcode.key] as? String ?? ""
        self.geo = Coordinates(json: json[Address.geo.key] as? JSON ?? JSON())
    }
    public var asJSON: JSON {
        return [Address.street.key: self.street,
                Address.suite.key: self.suite,
                Address.city.key: self.city,
                Address.zipcode.key: self.zipcode,
                Address.geo.key: self.geo.asJSON
        ]
    }
}

public func ==(lhs: Address, rhs: Address) -> Bool {
    return lhs.street == rhs.street
            && lhs.suite == rhs.suite
            && lhs.city == rhs.city
            && lhs.zipcode == rhs.zipcode
            && lhs.geo == rhs.geo
}