//
// Created by Bohdan Orlov on 09/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Post: Entity {
    public let id: Int
    public let userId: Int
    public let title: String
    public let body: String
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(id: Int, userId: Int, title: String, body: String) {
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
    }
}

extension Post {
    public static var id:Attribute<String> { return "id" }
    public static var userId:Attribute<String> { return "userId" }
    public static var title:Attribute<String> { return "title" }
    public static var body:Attribute<String> { return "body" }
}

extension Post {
    public init(json: JSON) {
        self.id = json[Post.id.key] as? Int ?? -1
        self.userId = json[Post.userId.key] as? Int ?? -1
        self.title = json[Post.title.key] as? String ?? ""
        self.body = json[Post.body.key] as? String ?? ""
    }
    public var asJSON: JSON {
        return [Post.id.key: self.id,
                Post.userId.key: self.userId,
                Post.title.key: self.title,
                Post.body.key: self.body
        ]
    }
}

public func ==(lhs: Post, rhs: Post) -> Bool {
    return lhs.id == rhs.id
            && lhs.userId == rhs.userId
            && lhs.title == rhs.title
            && lhs.body == rhs.body
}