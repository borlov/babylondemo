//
// Created by Bohdan Orlov on 09/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct User: Entity {
    public let id: Int
    public let name: String
    public let email: String
    public let address: Address
    public let phone: String
    public let website: String
    public let company: Company
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(id: Int, name: String, email: String, address: Address, phone: String, website: String, company: Company) {
        self.id = id
        self.name = name
        self.email = email
        self.address = address
        self.phone = phone
        self.website = website
        self.company = company
    }
}

extension User {
    public static var id:Attribute<Int> { return "id" }
    public static var name:Attribute<String> { return "name" }
    public static var email:Attribute<String> { return "email" }
    public static var address:Attribute<String> { return "address" }
    public static var phone:Attribute<String> { return "phone" }
    public static var website:Attribute<String> { return "website" }
    public static var company:Attribute<String> { return "company" }
}

extension User {
    public init(json: JSON) {
        self.id = json[User.id.key] as? Int ?? -1
        self.name = json[User.name.key] as? String ?? ""
        self.email = json[User.email.key] as? String ?? ""
        self.address = Address(json: json[User.address.key] as? JSON ?? JSON())
        self.phone = json[User.phone.key] as? String ?? ""
        self.website = json[User.website.key] as? String ?? ""
        self.company = Company(json: json[User.company.key] as? JSON ?? JSON())
    }
    public var asJSON: JSON {
        return [User.id.key: self.id,
                User.name.key: self.name,
                User.email.key: self.email,
                User.address.key: self.address.asJSON,
                User.phone.key: self.phone,
                User.website.key: self.website,
                User.company.key: self.company.asJSON,
        ]
    }
}

public func ==(lhs: User, rhs: User) -> Bool {
    return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.email == rhs.email
            && lhs.address == rhs.address
            && lhs.phone == rhs.phone
            && lhs.website == rhs.website
            && lhs.company == rhs.company
}