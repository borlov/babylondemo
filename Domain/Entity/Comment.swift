//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Comment: Entity {
    public let id: Int
    public let postId: Int
    public let name: String
    public let body: String
    public let email: String
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(id: Int, postId: Int, name: String, body: String, email: String) {
        self.id = id
        self.postId = postId
        self.name = name
        self.body = body
        self.email = email
    }
}

extension Comment {
    public static var id:Attribute<Int> { return "id" }
    public static var postId:Attribute<Int> { return "postId" }
    public static var name:Attribute<String> { return "name" }
    public static var body:Attribute<String> { return "body" }
    public static var email:Attribute<String> { return "email" }
}

extension Comment {
    public init(json: JSON) {
        self.id = json[Comment.id.key] as? Int ?? -1
        self.postId = json[Comment.postId.key] as? Int ?? -1
        self.name = json[Comment.name.key] as? String ?? ""
        self.body = json[Comment.body.key] as? String ?? ""
        self.email = json[Comment.email.key] as? String ?? ""
    }
    public var asJSON: JSON {
        return [Comment.id.key: self.id,
                Comment.postId.key: self.postId,
                Comment.name.key: self.name,
                Comment.body.key: self.body,
                Comment.email.key: self.email,
        ]
    }
}

public func ==(lhs: Comment, rhs: Comment) -> Bool {
    return lhs.id == rhs.id
            && lhs.postId == rhs.postId
            && lhs.name == rhs.name
            && lhs.body == rhs.body
            && lhs.email == rhs.email
}