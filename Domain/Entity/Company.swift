//
// Created by Bohdan Orlov on 09/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Company: Entity {
    public let name: String
    public let catchPhrase: String
    public let bs: String
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(name: String, catchPhrase: String, bs: String) {
        self.name = name
        self.catchPhrase = catchPhrase
        self.bs = bs
    }
}

extension Company {
    public static var name:Attribute<String> { return "name" }
    public static var catchPhrase:Attribute<String> { return "catchPhrase" }
    public static var bs:Attribute<String> { return "bs" }
}

extension Company {
    public init(json: JSON) {
        self.name = json[Company.name.key] as? String ?? ""
        self.catchPhrase = json[Company.catchPhrase.key] as? String ?? ""
        self.bs = json[Company.bs.key] as? String ?? ""
    }
    public var asJSON: JSON {
        return [Company.name.key: self.name,
                Company.catchPhrase.key: self.catchPhrase,
                Company.bs.key: self.bs,
        ]
    }
}

public func ==(lhs: Company, rhs: Company) -> Bool {
    return lhs.name == rhs.name
            && lhs.catchPhrase == rhs.catchPhrase
            && lhs.bs == rhs.bs
}