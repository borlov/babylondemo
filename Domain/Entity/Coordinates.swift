//
// Created by Bohdan Orlov on 09/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Coordinates: Entity {
    public let latitude: Double
    public let longitude: Double
    // struct's memberwise initalizer is 'internal', but we need 'public'
    public init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

extension Coordinates {
    static var latitude:Attribute<String> { return "latitude" }
    static var longitude:Attribute<String> { return "longitude" }
}

extension Coordinates {
    public init(json: JSON) {
        self.latitude = json[Coordinates.latitude.key] as? Double ?? -1
        self.longitude = json[Coordinates.longitude.key] as? Double ?? -1
    }
    public var asJSON: JSON {
        return [Coordinates.latitude.key: self.latitude,
                Coordinates.longitude.key: self.longitude
        ]
    }
}

public func ==(lhs: Coordinates, rhs: Coordinates) -> Bool {
    return lhs.latitude == rhs.latitude
            && lhs.longitude == rhs.longitude
}