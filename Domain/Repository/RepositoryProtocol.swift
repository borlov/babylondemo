//
// Created by Bohdan Orlov on 11/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public enum FetchResult<T> {
    case some([T], fromCache: Bool)
    case error(Error)
}

public protocol RepositoryProtocol {
    func fetch<E: Entity>(completion: @escaping (FetchResult<E>) -> Void)
    func fetch<E: Entity>(predicate: NSPredicate?, completion: @escaping (FetchResult<E>) -> Void)
}