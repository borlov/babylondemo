//
// Created by Bohdan Orlov on 11/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import Foundation

public struct Request {
    public let endpoint: String
    public init(endpoint: String) {
        self.endpoint = endpoint
    }
}

public enum Response {
    case success(Data)
    case failure(Error)
}

public protocol NetworkServiceTask {
    var url: URL { get }
}

public protocol NetworkServiceProtocol {
    @discardableResult func send(request: Request,  completionHandler: @escaping (Response) -> Void) -> NetworkServiceTask
}