import Foundation

/// Returns an equality predicate for the two given expressions
public func == (left: NSExpression, right: NSExpression) -> NSPredicate {
  return NSComparisonPredicate(leftExpression: left, rightExpression: right, modifier: NSComparisonPredicate.Modifier.direct, type: NSComparisonPredicate.Operator.equalTo, options: NSComparisonPredicate.Options(rawValue: 0))
}
