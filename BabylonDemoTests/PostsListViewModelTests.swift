//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import XCTest
import Domain
import Platform
import BabylonDemo

class PostsListViewModelTests: XCTestCase {
    func testWhenViewModelAppears_ThenPostsAreFetched() {
        // Given
        var fetchPostsCalled = false
        let fakeRepository = StubRepository()
        fakeRepository.fetchPostsClosure = { _ in
            fetchPostsCalled = true
        }
        let viewModel = PostsListViewModel(repository: fakeRepository)
        // When
        viewModel.willAppear()
        // Then
        XCTAssertTrue(fetchPostsCalled)
    }

    func testWhenViewModelAppears_ThenPostsAreEventuallyDelivered() {
        var viewModelDidChangeCalled = false
        let fakeRepository = StubRepository()
        let viewModel = PostsListViewModel(repository: fakeRepository)
        viewModel.didChange = { _ in
            viewModelDidChangeCalled = true
        }
        viewModel.willAppear()
        XCTAssertTrue(viewModelDidChangeCalled)
        XCTAssertTrue(viewModel.postTitles.count == 1)
    }

    func testWhenSelectPostTitleAtIndex_ThenPostDetailsViewModelWithAppropriatePostIsReturned() {
        let fakeRepository = StubRepository()
        let viewModel = PostsListViewModel(repository: fakeRepository)
        viewModel.willAppear()
        let postDetailsViewModel = viewModel.detailsViewModetForSelectedPostTitle(at: 0)
        XCTAssertEqual(postDetailsViewModel.post, post)
    }
}

class StubRepository: RepositoryProtocol {
    var fetchPostsClosure: ((NSPredicate?) -> Void)?
    var fetchUserClosure: ((NSPredicate?) -> Void)?
    var fetchCommentClosure: ((NSPredicate?) -> Void)?
    func fetch<E: Entity>(completion: @escaping (FetchResult<E>) -> Void) {
        fetch(predicate: nil, completion: completion)
    }

    func fetch<E:Entity>(predicate: NSPredicate?, completion: @escaping (FetchResult<E>) -> Void) {
        // it is not possible to have generic closure
        switch E.self {
            case is Post.Type:
                self.fetchPostsClosure?(predicate)
                completion(FetchResult.some([E.init(json:post.asJSON)], fromCache: false))
            case is User.Type:
                self.fetchUserClosure?(predicate)
                completion(FetchResult.some([E.init(json:user.asJSON)], fromCache: false))
            case is Comment.Type:
                self.fetchCommentClosure?(predicate)
                completion(FetchResult.some([E.init(json:comment.asJSON)], fromCache: false))
            default: XCTFail()
        }
        // for the same reason stub Entity is created here and not injected from test

    }

}
