//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import XCTest
import Domain
import Platform

class RepositoryTests: XCTestCase {
    var stubNetworkService = StubNetworkService()
    var cacheStorage: UserDefaults!
    var repository: Repository!
    override func setUp() {
        super.setUp()
        self.stubNetworkService = StubNetworkService()
        self.cacheStorage = UserDefaults()
        self.repository = Repository(networkService: self.stubNetworkService, cacheStorage: self.cacheStorage)
    }

    override func tearDown() {
        super.tearDown()
        let appDomain = Bundle.main.bundleIdentifier!
        self.cacheStorage.removePersistentDomain(forName: appDomain)
    }


    func testWhenFetchIsCalled_ThenNetworkRequestIsSend() {
        var sendWasCalled = false
        self.stubNetworkService.sendClosure = { _ in
            sendWasCalled = true
        }
        self.repository.fetch { (result: FetchResult<Post>) in
        }
        XCTAssertTrue(sendWasCalled)
    }

    func testWhenPostFetchIsCalled_ThenRequestEndpointIsValid() {
        self.stubNetworkService.sendClosure = { request, _ in
            XCTAssertEqual(request.endpoint, "posts")
        }
        self.repository.fetch { (result: FetchResult<Post>) in
        }
    }

    func testWhenRequestCompletionIsCalled_ThenRepositoryCompletionIsCalled() {
        self.stubNetworkService.sendClosure = { _, completion in
            completion(Response.success(Data()))
        }
        var fetchCompletionCalled = false
        self.repository.fetch { (result: FetchResult<Coordinates>) in
            fetchCompletionCalled = true
        }
        XCTAssertTrue(fetchCompletionCalled)
    }

    func testWhenFetchCompletionIsCalled_ThenRepositoryReturnsEntities() {
        let coordinates = Coordinates(latitude: 15, longitude: -30)
        self.stubNetworkService.sendClosure = { _, completion in
            let coordinatesJSON = coordinates.asJSON
            let data = try! JSONSerialization.data(withJSONObject: coordinatesJSON)
            completion(Response.success(data))
        }
        self.repository.fetch { (result: FetchResult<Coordinates>) in
            switch result {
                case .some(let resultCoordinates, _): XCTAssertEqual(resultCoordinates, [coordinates])
                default: XCTFail()
            }
        }
    }

    func testWhenFetch_ThenEntitiesAreReturnedFromCacheFirst() {
        let coordinates = Coordinates(latitude: 15, longitude: -30)
        self.cacheStorage.setValue([coordinates.asJSON], forKey: "coordinates")
        self.stubNetworkService.sendClosure = { request, completion in
            let coordinatesJSON = coordinates.asJSON
            let data = try! JSONSerialization.data(withJSONObject: coordinatesJSON)
            completion(Response.success(data))
        }
        var returnedFromCache = false
        var returnedFromRemote = false
        self.repository.fetch { (result: FetchResult<Coordinates>) in
            switch result {
                case .some(let resultCoordinates, fromCache: true):
                    XCTAssertEqual(resultCoordinates, [coordinates])
                    returnedFromCache = true
                    XCTAssertFalse(returnedFromRemote)
                case .some(let resultCoordinates, fromCache: false):
                    XCTAssertEqual(resultCoordinates, [coordinates])
                    returnedFromRemote = true
                default: XCTFail()
            }
        }
        XCTAssertTrue(returnedFromCache)
        XCTAssertTrue(returnedFromRemote)
    }

    func testWhenFetchWithPredicate_ThenEntitiesAreFiltered() {
        let comments = [Comment(id: 1, postId: 1, name: "n", body: "b", email: "e"),
                        Comment(id: 2, postId: 1, name: "n", body: "b", email: "e"),
                        Comment(id: 3, postId: 2, name: "n", body: "b", email: "e")]
        self.stubNetworkService.sendClosure = { _, completion in
            let commentsJSON = comments.map { $0.asJSON }
            let data = try! JSONSerialization.data(withJSONObject: commentsJSON)
            completion(Response.success(data))
        }
        self.repository.fetch(predicate: Comment.postId == 1) { (result: FetchResult<Comment>) in
            switch result {
            case .some(let resultComments, _): XCTAssertEqual(resultComments, Array(comments[0...1]))
            default: XCTFail()
            }
        }
    }
}


class StubNetworkService: NetworkServiceProtocol {
    var sendClosure: ((Request, (Response) -> Void) -> Void)?
    func send(request: Request, completionHandler: @escaping (Response) -> Void) -> NetworkServiceTask {
        sendClosure?(request, completionHandler)
        return URLSessionDataTask()
    }

}
