//
// Created by Bohdan Orlov on 11/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import XCTest
import Domain
import Platform
import BabylonDemo


let post = Post(id: 1, userId: 2, title: "Stuff", body: "Done")
let coordinates = Coordinates(latitude: 15.0, longitude: -30.0)
let address = Address(street: "one st", suite: "3", city: "London", zipcode: "nw", geo: coordinates)
let company = Company(name: "Inc", catchPhrase: "get it done", bs: "bulls")
let user = User(id: 1, name: "John", email: "example@gmail.com", address: address, phone: "+44123", website: "web.com", company: company)
let comment = Comment(id: 1, postId: 2, name: "caption", body: "long text", email: "example@gmail.com")

class PostDetailsViewModelTests: XCTestCase {

    func testWhenViewModelAppears_ThenAuthorIsLoaded() {
        let repository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: repository)
        var authorIsFetched = false
        repository.fetchUserClosure = { predicate in
            XCTAssertEqual(predicate, User.id == post.userId)
            authorIsFetched = true
        }
        viewModel.willAppear()
        XCTAssertTrue(authorIsFetched)
    }

    func testWhenViewModelAppears_ThenCommentsAreLoaded() {
        let repository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: repository)
        var commentsAreFetched = false
        repository.fetchCommentClosure = { predicate in
            XCTAssertEqual(predicate, Comment.postId == post.id)
            commentsAreFetched = true
        }
        viewModel.willAppear()
        XCTAssertTrue(commentsAreFetched)
    }

    func testWhenViewModelAppears_ThenCommentsAreEventuallyDelivered() {
        var viewModelDidChangeCalled = false
        let fakeRepository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: fakeRepository)
        viewModel.didChange = { _ in
            viewModelDidChangeCalled = true
        }
        viewModel.willAppear()
        XCTAssertTrue(viewModelDidChangeCalled)
        XCTAssertTrue(viewModel.commentsCount > 0)
    }

    func testWhenViewModelCreated_ThenPostDescriptionAvailableImmediately() {
        let repository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: repository)
        XCTAssert(viewModel.postDescription.characters.count > 0)
    }

    func testWhenViewModelAppears_ThenAuthorNameIsAvailableEventually() {
        let repository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: repository)
        viewModel.willAppear()
        XCTAssertEqual(viewModel.authorName, user.name)
    }

    func testWhenViewModelAppears_ThenCommentsCountIsAvailableEventually() {
        let repository = StubRepository()
        let viewModel = PostDetailsViewModel(post: post, repository: repository)
        viewModel.willAppear()
        XCTAssertGreaterThan(viewModel.commentsCount, 0)
    }
}
