//
//  NetworkServiceTests.swift
//  NetworkServiceTests
//
//  Created by Bohdan Orlov on 08/12/2016.
//  Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import XCTest
@testable import Domain
import Platform

class NetworkServiceTests: XCTestCase {
    private var hostURL = URL(string: "http://api.com")!
    private var stubSession: StubURLSession!
    private var networkService: NetworkService!
    override func setUp() {
        super.setUp()
        self.stubSession = StubURLSession()
        self.networkService = NetworkService(hostURL: self.hostURL, session: self.stubSession)
    }
    
    func testWhenSendRequest_ThenItIsSendViaSession() {
        // Given
        var requestWasSent = false
        self.stubSession.dataTaskClosure = { url, completionHandler in
            let fakeTask = StubURLSessionDataTask()
            fakeTask.resumeClosure = { requestWasSent = true }
            return fakeTask
        }
        // When
        _ = self.networkService.send(request: Request(endpoint: "")) { _ in }
        // Then
        XCTAssertTrue(requestWasSent)
    }

    func testGivenNoData_WhenSendRequest_ThenCallbackIsCalledWithError() {
        // Given
        self.stubSessionCompletion(with: nil)
        var completionCalledWithError = false
        let expectation = self.expectation(description: #function)
        // When
        _ = self.networkService.send(request: Request(endpoint: "")) { response in
            switch response {
                case .failure(_): completionCalledWithError = true
                case .success(_): break
            }
            expectation.fulfill()
        }
        // Then
        self.waitForExpectations(timeout: 0.1, handler: nil)
        XCTAssertTrue(completionCalledWithError)
    }

    func testGivenSomeData_WhenSendRequest_ThenCallbackIsCalledWithData() {
        // Given
        let stubData = Data(base64Encoded: "data")
        self.stubSessionCompletion(with: stubData)
        var completionCalledWithData = false
        let expectation = self.expectation(description: #function)
        // When
        _ = self.networkService.send(request: Request(endpoint: "")) { response in
            switch response {
                case .failure(_): break
                case .success(let data): completionCalledWithData = data == stubData
            }
            expectation.fulfill()
        }
        // Then
        self.waitForExpectations(timeout: 0.1, handler: nil)
        XCTAssertTrue(completionCalledWithData)
    }

    func testGivenSomeData_WhenSendRequest_ThenCallbackIsCalledOnMainThread() {
        // Given
        self.stubSession.dataTaskClosure = { url, completionHandler in
            DispatchQueue.global().async {
                completionHandler(nil, HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
            }
            return StubURLSessionDataTask()
        }
        let expectation = self.expectation(description: #function)
        // When
        _ = self.networkService.send(request: Request(endpoint: "")) { _ in
            XCTAssertTrue(Thread.isMainThread)
            expectation.fulfill()
        }
        // Then
        self.waitForExpectations(timeout: 0.1, handler: nil)
    }

    func testGivenHostAndRequestURL_WhenSendRequest_ThenThoseUsedInURLRequest() {
        // Given
        self.stubSession.dataTaskClosure = { url, completionHandler in
            let fakeTask = StubURLSessionDataTask()
            fakeTask.currentRequestClosure = { return URLRequest(url: url) }
            completionHandler(nil, HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
            return fakeTask
        }
        // When
        let endpoint = "data"
        let request = Request(endpoint: endpoint)
        let task = self.networkService.send(request: request) { _ in
        }
        // Then
        let expectedURL = self.hostURL.appendingPathComponent(endpoint)
        XCTAssertTrue(task.url == expectedURL)
    }

    func stubSessionCompletion(with data: Data?) {
        self.stubSession.dataTaskClosure = { url, completionHandler in
            let fakeTask = StubURLSessionDataTask()
            completionHandler(data, HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
            return fakeTask
        }
    }
}

class StubURLSession: URLSessionProtocol {
    var dataTaskClosure: ((URL, @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask)?
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTaskClosure!(url, completionHandler)
    }
}

class StubURLSessionDataTask: URLSessionDataTask {
    var resumeClosure: (() -> Void)?
    override func resume() {
        self.resumeClosure?()
    }

    var currentRequestClosure: (() -> URLRequest?)?
    override var currentRequest: URLRequest? {
        return currentRequestClosure!()
    }

}
