//
// Created by Bohdan Orlov on 10/12/2016.
// Copyright (c) 2016 BohdanOrlov. All rights reserved.
//

import XCTest
import Domain

let comment = Comment(id: 1, postId: 2, name: "caption", body: "long text", email: "example@gmail.com")
let commentJSON: JSON = ["id": 1, "postId": 2, "name": "caption", "body": "long text", "email": "example@gmail.com"]

let coordinates = Coordinates(latitude: 15.0, longitude: -30.0)
let coordinatesJSON: JSON = ["latitude": 15.0, "longitude": -30.0]

let address = Address(street: "one st", suite: "3", city: "London", zipcode: "nw", geo: coordinates)
let addressJSON: JSON = ["street": "one st", "suite": "3", "city": "London", "zipcode": "nw", "geo": coordinatesJSON]

let company = Company(name: "Inc", catchPhrase: "get it done", bs: "bulls")
let companyJSON = ["name": "Inc", "catchPhrase": "get it done", "bs": "bulls"]

let post = Post(id: 1, userId: 2, title: "Stuff", body: "Done")
let postJSON: JSON = ["id": 1, "userId": 2, "title": "Stuff", "body": "Done"]

let user = User(id: 1, name: "John", email: "example@gmail.com", address: address, phone: "+44123", website: "web.com", company: company)
let userJSON: JSON = ["id": 1, "name": "John", "email": "example@gmail.com", "address": addressJSON, "phone": "+44123", "website": "web.com", "company": companyJSON]


class JSONSerializationTests: XCTestCase {

    func testWhenCommentIsSerialized_ThenJSONIsValid() {
        let serializedComment = comment.asJSON
        XCTAssertEqual(serializedComment as NSDictionary, commentJSON as NSDictionary)
    }

    func testWhenCommentIsDeserialized_ThenEntityIsValid() {
        let deserializedComment = Comment(json: commentJSON)
        XCTAssertEqual(deserializedComment, comment)
    }

    func testWhenCoordinatesIsSerialized_ThenJSONIsValid() {
        let serializedСoordinates = coordinates.asJSON
        XCTAssertEqual(serializedСoordinates as NSDictionary, coordinatesJSON as NSDictionary)
    }

    func testWhenCoordinatesIsDeserialized_ThenEntityIsValid() {
        let deserializedCoordinates = Coordinates(json: coordinatesJSON)
        XCTAssertEqual(deserializedCoordinates, coordinates)
    }

    func testWhenAddressIsSerialized_ThenJSONIsValid() {
        let serializedAddress = address.asJSON
        XCTAssertEqual(serializedAddress as NSDictionary, addressJSON as NSDictionary)
    }

    func testWhenAddressIsDeserialized_ThenEntityIsValid() {
        let deserializedAddress = Address(json: addressJSON)
        XCTAssertEqual(deserializedAddress, address)
    }

    func testWhenCompanyIsSerialized_ThenJSONIsValid() {
        let serializedCompany = company.asJSON
        XCTAssertEqual(serializedCompany as NSDictionary, companyJSON as NSDictionary)
    }

    func testWhenCompanyIsDeserialized_ThenEntityIsValid() {
        let deserializedCompany = Company(json: companyJSON)
        XCTAssertEqual(deserializedCompany, company)
    }

    func testWhenPostIsSerialized_ThenJSONIsValid() {
        let serializedPost = post.asJSON
        XCTAssertEqual(serializedPost as NSDictionary, postJSON as NSDictionary)
    }

    func testWhenPostIsDeserialized_ThenEntityIsValid() {
        let deserializedPost = Post(json: postJSON)
        XCTAssertEqual(deserializedPost, post)
    }

    func testWhenUserIsSerialized_ThenJSONIsValid() {
        let serializedUser = user.asJSON
        XCTAssertEqual(serializedUser as NSDictionary, userJSON as NSDictionary)
    }

    func testWhenUserIsDeserialized_ThenEntityIsValid() {
        let deserializedUser = User(json: userJSON)
        XCTAssertEqual(deserializedUser, user)
    }


}
